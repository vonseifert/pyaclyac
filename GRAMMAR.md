# Introduction
For this introduction we will use the word pyan, which means love. 
For pronunciation refer to phonetic.md
Mostly Pyash conforms to International Phonetic Alphabet,
except that c is the post-alveolar fricative. In English the `sh' sound. 
The y is palatal approximant. In English the `y' in `you'.

The vowels for English readers I'll explain here:
* i -- this is the closed front unrounded vowel. In English "eee".
* a -- this the open central unrounded vowel. In English "ah". 
* u -- this is the back closed rounded vowel. In English "oo" as in "moo". 
* o -- this is close mid rounded vowel. In English "oh". 
* 6 -- this is the scwha or mid central unrounded vowel. In English "uh".

There are also two tones:
* 7 -- this is a high pitch tone
* 2 -- this a low pitch tone
unmarked words are in medium tone. 

The prosodic cadence is indicated through loudness, with primary stress on initial syllable of a word, with secondary stress on every odd syllable therafter.

# Grammatical Mood
Grammatical mood tells a person if the sentence is declaring a fact, a possibility, a command, a question or otherwise. 

The basic grammatical moods are:

* li -- realis mood, for declaring a fact. In some ways it is similar to a period. For example: pyanli "love.".
* tu -- deontic mood, for expressing a command. For example: pyantu "love!".
* si  -- epistemic mood, for expressing a possibility. For example: pyansi "may love.".
* psi -- optatative mood, for expressing a request. For example: pyanpsi "please love.".
* ri -- interrogative mood, for expressing. For example: pyanri "love?"

# Pronouns

Pronouns are for indicating a person.
The basic pronouns are:

* mi -- me/I. For example: mi pyanli "I love."
* sa -- you/thee. For example: sa pyanli "You love."
* ya -- it.  For example: ya pyanli "It love."
* ke -- question word, who/what/where/when depending on context. ke pyanri "what love?"

# Grammatical Cases
Grammatical cases are for indicating how a noun phrase is related to the verb phrase. Brief history of cases, is that in human languages cases used to be verbs or adjectives which became abbreviated over time allowing for more complicated sentences.

The basic cases are 

* na -- which is the nominative case or subject. For example: mina pyanli "I love.".
* ka -- which is the accusative case or object. For example: saka pyanli "You be loved.".
* ti -- which is the genitive case, like "of", or "'s". For example: miti pyanka "my love".

An example that uses both: mina saka pyanli "I love you.".

Lets add several more stems to work with. 

* kwan -- light
* kwin -- give
* tlan -- God
* cyit -- heart

## Motion


* so -- source, roughly "from". mina tlanso li "I am from God.".
* te -- location, roughly "at". mina kwante li "I am at light.".
* ga -- way, through, roughly "via". cyitga pyanli "via heart be love.".
* ma -- destination, roughly "to". For example: tlanna sama pyanka kwinli "God, to you, love, gives.".

## Context
Cases can also be used to express relationship within several deixis or contexts, we will cover some of the most common.

Lets add a couple more words
* tlap -- floor
* hrat -- roof

* ce -- under context, "under". For example: mina hratce li "I be under roof.".
* nu -- interior context, "in". For example: sana cyitnu keka ri "you, in heart, what?".
* pi -- surface context, "on". For example: mina tlappi li "I be on floor.".
* se -- time context, indicating something in time. For example: kese pyanri "when love?"
* to -- space context, indicating someting in space. For example: keto kwanri "where light?"
* pa -- person context, indicating a space/time or time/space person or event. For example: kepa pyanri "Who be love?"

contexts and cases can also be combined. as a general rule, start with the context, end with the case. 

Lets add another couple words:

* hkin -- go
* hlaf -- stay
* hlam -- come
* hcas -- house

* numa -- into. For example: mina hcasnuma hkinsi "I may go into house.".
* piso -- out from. For example: mina hcaspiso hlamsi "I may come out from house.".
* pama -- for someone. For example: tlanpama hlaftu "stay for God."
* paso -- because of. For example: tlanpaso kwanka li "because of God, be light".


# Quantifiers
